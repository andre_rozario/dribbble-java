# dribbble-Java

**Sistema web de consulta na API do Dribbble**

* Java 8
* Spring Boot 1.4.1
* Spring Data 1.10.3
* MySQL Connector 5.1.39
* Angular 1.5.7
* Angular Material 1.1.0
* Bootstrap 3.3.7
* JQuery 2.2.4.2.4

**Configurações do projeto**

Nome do banco: desafio

Usuário do banco: root

Sem senha

Endereço de acesso após iniciar a aplicação: http://localhost:8080/desafio/#/