app.controller('bookmarkController', [
	
    '$scope', '$rootScope', 'bookmarkService', '$mdDialog', '$mdToast', 
    
    function ($scope, $rootScope, bookmarkService, $mdDialog, $mdToast) {
    	
        var vm = this;
        
        vm.bookmarks = [];
        vm.bookmark = {};
        
        vm.totalPages = null;
        
        vm.type = '';
        
        vm.successMessage = '';
        vm.errorMessage = '';
        
        vm.page = 0;
        
        $rootScope.offset = true;
        
        // Bookmarks
        
        $scope.loadByType = function(ev, type) {
        	
        	console.log('Type of Select', type);
        	
        	vm.type = type;
        	
        	getBookmarks(0, vm.type);
        	
        	vm.bookmarks = [];
        	vm.page = 0;
        }
        
        
        function getBookmarks(page, type){
        	
        	$scope.loading = true;
        	
        	bookmarkService.getBookmarks(page, type)
        	.then(
                function(response) {
                	
                	vm.bookmarks = vm.bookmarks.concat(response.content);
                	
                	vm.totalPages = response.totalPages;
                },
                function(error){
                	
                    console.error('Error while fetching Bookmarks', error);
                }
            )
            .finally(function() {
            	
                $scope.loading = false;
            });
        }
        
        $scope.loadMoreBookmarks = function(){
        	
        	if(vm.page < (vm.totalPages - 1)) {
        		
        		getBookmarks(++vm.page, vm.type);
        		
        	} else {
        		
        		vm.page = vm.totalPages - 1;
        	}
        }
        
        // Dialog - Bookmark Detail
        
        $scope.status = '';
        $scope.customFullscreen = false;
        
        $scope.showBookmarkDetails = function(ev, bookmark) {
        	
        	vm.bookmark = bookmark;
        	
            $mdDialog.show({
            	controller: DialogController,
            	templateUrl: 'views/shots/bookmarks/_bookmark-dialog.tmpl.html',
            	locals: {
            		bookmark: vm.bookmark
            	},
            	parent: angular.element(document.body),
            	targetEvent: ev,
            	clickOutsideToClose:true,
            	fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
          
        function DialogController($scope, $mdDialog, bookmark) {
        	
        	$scope.bookmark = bookmark;
        	
        	$scope.hide = function() {
        		$mdDialog.hide();
        	};
        	  
        	$scope.cancel = function() {
        		$mdDialog.cancel();
        	};
        	
        	$scope.removeBookmark = function(ev, id) {
            	
            	console.log('Removing Bookmark', id);
            	
            	deleteBookmark(id);
        	}
        }
        
        function deleteBookmark(id) {
        	
            console.log('About to delete bookmark');
            
            bookmarkService.deleteBookmark(id)
                .then(
                    function (response) {
                    	
                        console.log('Bookmark deleted successfully');
                        
                        vm.successMessage = 'Essa imagem foi excluída da sua galeria de favoritos com sucesso';
                        vm.errorMessage = '';
                        vm.bookmark = {};
                        
                        $mdToast.show(
                        	      $mdToast.simple()
                        	      	.theme('green')
                        	        .textContent(vm.successMessage)
                        	        .position('top right')
                        	        .hideDelay(3000)
                        	    );
                        
                        
                        // Limpa e recarrega a(s) página(s) sem o Bookmark removido
                        
                        pageReload();
                    },
                    function (error) {
                    	
                        console.error('Error while deleting Bookmark');
                        
                        vm.errorMessage = 'Erro ao tentar excluir o Bookmark';
                        vm.successMessage = '';
                        
                        $mdToast.show(
                      	      $mdToast.simple()
                      	      	.theme('red')
                      	        .textContent(vm.errorMessage)
                      	        .position('top right')
                      	        .hideDelay(3000)
                      	    );
                    }
                );
        }
        
        function pageReload() {
        	
        	vm.bookmarks = [];
            
            for(var i = 0; i <= (vm.page); i++){
                
            	getBookmarks(i, vm.type);
            }
        }
    }
]);

app.directive('infinitScrollPage', function () {
	
    return {
        
    	restrict: 'A',
        
        link: function (scope, element, attr) {
        	
            var elm = $(document);
            
            elm.bind('scroll', function () {
            	
                var porcentagem = (($(window).scrollTop() + $(window).height()) / $(this).height()).toFixed(2);
                
                if (porcentagem >= 1) {
                	
                    scope.$apply(attr.infinitScrollPage);
                }
            });
        }
    };
});