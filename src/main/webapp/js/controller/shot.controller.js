app.controller('shotController', [
	
    '$scope', '$rootScope', 'shotService', '$mdDialog', '$mdToast',
    
    function ($scope, $rootScope, shotService, $mdDialog, $mdToast) {
    	
        var vm = this;
        
        vm.shots = [];
        vm.shot = {};
        
        vm.successMessage = '';
        vm.errorMessage = '';
        
        vm.page = 1;
        
        $rootScope.offset = false;
        
        // Shots
        
        getShots(vm.page);
        
        function getShots(page){
        	
        	$scope.loading = true;
        	
        	shotService.getShots(page)
        	.then(
                function(response) {
                	
                	vm.shots = vm.shots.concat(response);
                },
                function(error){
                	
                    console.error('Error while fetching Shots', error);
                }
            )
            .finally(function() {
            	
                $scope.loading = false;
            });
        }
        
        $scope.loadMoreShots = function(){
        	
        	getShots(++vm.page);
        }
        
        // Dialog - Shot Detail
        
        $scope.status = '';
        $scope.customFullscreen = false;
        
        $scope.showShotDetails = function(ev, shot) {
        	
        	vm.shot = shot;
        	
            $mdDialog.show({
            	controller: DialogController,
            	templateUrl: 'views/shots/_shot-dialog.tmpl.html',
            	locals: {
            		shot: vm.shot
            	},
            	parent: angular.element(document.body),
            	targetEvent: ev,
            	clickOutsideToClose:true,
            	fullscreen: $scope.customFullscreen // Only for -xs, -sm breakpoints.
            });
        };
          
        function DialogController($scope, $mdDialog, shot) {
        	
        	$scope.shot = shot;
        	
        	$scope.hide = function() {
        		$mdDialog.hide();
        	};
        	  
        	$scope.cancel = function() {
        		$mdDialog.cancel();
        	};
        	
        	$scope.addBookmark = function(ev, shot) {
            	
            	console.log('Saving New Shot', shot);
            	
            	var newShot = {
            			id: shot.id, 
            			title: shot.title, 
            			imgNormalUrl: shot.images.normal, 
            			imgHidpiUrl: shot.images.hidpi,
            			viewsCount: shot.views_count, 
            			bucketsCount: shot.buckets_count, 
            			likesCount: shot.likes_count, 
            			attachmentsCount: shot.attachments_count, 
            			author: shot.user.name, 
            	}
            	
            	saveBookmark(newShot);
            }
        }
        
        function saveBookmark(shot) {
        	
            console.log('About to create shot');
            
            shotService.saveBookmark(shot)
                .then(
                    function (response) {
                    	
                        console.log('Shot created successfully');
                        
                        vm.successMessage = 'Imagem salva com sucesso na sua galeria de imagens';
                        vm.errorMessage = '';
                        vm.shot = {};
                        
                        $mdToast.show(
                        	      $mdToast.simple()
                        	      	.theme('green')
                        	        .textContent(vm.successMessage)
                        	        .position('top right')
                        	        .hideDelay(3000)
                        	    );
                    },
                    function (error) {
                    	
                        console.error('Error while creating Shot');
                        
                        vm.errorMessage = error.data.errorMessage;
                        vm.successMessage = '';
                        
                        $mdToast.show(
                      	      $mdToast.simple()
                      	      	.theme('red')
                      	        .textContent(vm.errorMessage)
                      	        .position('top right')
                      	        .hideDelay(3000)
                      	    );
                    }
                );
        }
    }
]);

app.directive('infinitScrollPage', function () {
	
    return {
        
    	restrict: 'A',
        
        link: function (scope, element, attr) {
        	
            var elm = $(document);
            
            elm.bind('scroll', function () {
            	
                var porcentagem = (($(window).scrollTop() + $(window).height()) / $(this).height()).toFixed(2);
                
                if (porcentagem >= 1) {
                	
                    scope.$apply(attr.infinitScrollPage);
                }
            });
        }
    };
});