var app = angular.module("app", [ 'ngRoute', 'ngResource', 'ngMaterial']).run(function($rootScope) {
	
	$rootScope.resetOffset = function(ev, offset) {
		
		$rootScope.offset = offset;
	}
});

app.config(function($routeProvider){
	
    $routeProvider
        .when('/shots',{
    		templateUrl:'views/shots/shots.html',
    		controller:'shotController'
        })
        .when('/shots/bookmarks',{
    		templateUrl:'views/shots/bookmarks/bookmarks.html',
    		controller:'bookmarkController'
        })
        .otherwise(
            { redirectTo: '/'}
        );
});