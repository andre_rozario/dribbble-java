app.factory('shotService', function($http, $q) {
	
	var URI = 'http://localhost:8080/desafio/shots/';
	
	var service = {
			getShots: getShots, 
			saveBookmark: saveBookmark, 
	};

	return service;

	function getShots(page){
		
		var deferred = $q.defer();
		
		$http.get(URI+page)
		.then(
				function (response) {
					
					deferred.resolve(response.data);
				},
				function(error){
					
					console.error('Error while fetching Shots', error);
					
					deferred.reject(error);
				}
		);
		
		return deferred.promise;
	}

	function saveBookmark(shot) {
		
		var deferred = $q.defer();
		
		$http.post(URI, shot)
		.then(
				function (response) {
					
					deferred.resolve(response.data);
				},
				function(error){
					
					console.error('Error while creating Shot', error);
					
					deferred.reject(error);
				}
		);
		
		return deferred.promise;
	}

});