app.factory('bookmarkService', function($http, $q) {
	
	var URI = 'http://localhost:8080/desafio/shots/bookmarks';
	
	var service = {
			getBookmarks:getBookmarks,
			deleteBookmark:deleteBookmark,
	};

	return service;
	
	function getBookmarks(page, type){
		
		var deferred = $q.defer();
		
		var parameters = '?page='+page+'&size=12&type='+type
		
		$http.get(URI+parameters)
		.then(
				function (response) {
					
					deferred.resolve(response.data);
				},
				function(error){
					
					console.error('Error while fetching Bookmarks', error);
					
					deferred.reject(error);
				}
		);
		
		return deferred.promise;
	}
	
	function deleteBookmark(id) {
	
	    var deferred = $q.defer();
	    
	    $http.delete(URI+'/bookmark/'+id)
	    .then(
	        function (response) {
	        	
	            deferred.resolve(response.data);
	        },
	        function(error){
	        	
	            console.error('Error while deleting Bookmark');
	            
	            deferred.reject(error);
	        }
	    );
	    
	    return deferred.promise;
	}
});