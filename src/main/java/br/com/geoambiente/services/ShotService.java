package br.com.geoambiente.services;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.geoambiente.models.Shot;
import br.com.geoambiente.repositories.ShotRepository;

@Service
@Transactional
public class ShotService {

	@Autowired
	private ShotRepository shotRepository;
	
	public ShotService(ShotRepository shotRepository) {
		
		this.shotRepository = shotRepository;
	}
	
	@Transactional(readOnly = true)
	public Shot findOne(Integer id) {
		
		return this.shotRepository.findOne(id);
	}

	@Transactional(readOnly = true)
	public long count() {
		
		return this.shotRepository.count();
	}

	@Transactional(readOnly = true)
	public boolean exists(Integer id) {
		
		return this.shotRepository.exists(id);
	}
	
	@Transactional(readOnly = true)
	public Page<Shot> paginate(Pageable pageable) {
		
		return this.shotRepository.findAll(pageable);
	}

	@Transactional(readOnly = true)
	public Page<Shot> paginate(Specification<Shot> spec, Pageable pageable) {
		
		return this.shotRepository.findAll(spec, pageable);
	}
	
	public <S extends Shot> S save(S entity) {
		
		return this.shotRepository.save(entity);
	}
	
	public void delete(Shot shot) {
		
		this.shotRepository.delete(shot);
	}
	
	public Page<Shot> findByCreatedAtOrderByDesc(Pageable pageable) {
		
		return this.shotRepository.findByOrderByCreatedAtDesc(pageable);
	}
	
	public Page<Shot> findByLastMinutes(LocalDateTime from, LocalDateTime to, Pageable pageable) {
		
		return this.shotRepository.findByLastMinutes(from, to, pageable);
	}
}