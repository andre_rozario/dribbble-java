package br.com.geoambiente.controllers;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import br.com.geoambiente.models.Shot;
import br.com.geoambiente.services.ShotService;
import br.com.geoambiente.utils.CustomErrorType;

@RestController
@RequestMapping("/shots")
public class ShotController {
	
	private static final String ACCESS_TOKEN ="6324df57dddd0810b38fc4b087c91a86a0a8a93cd6fabed0fd8136110f561176";
	
	@Autowired
	private ShotService shotService;
	
	@GetMapping(value = "/{page}")
	public ResponseEntity<List<?>> getShots(@PathVariable("page") Integer page) {

		StringBuilder url = new StringBuilder();
		
		url.append("https://api.dribbble.com/v1/shots?sort=views&page=").append(page).append("&access_token=").append(ACCESS_TOKEN);
		
		RestTemplate restTemplate = new RestTemplate();
		
		List<?> shots = restTemplate.getForObject(url.toString(), List.class);
		
		return new ResponseEntity<List<?>>(shots, HttpStatus.OK);
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveBookmark(@RequestBody @Valid Shot shot, BindingResult result) {
		
		if (result.hasErrors()) {
			
			return new ResponseEntity(new CustomErrorType(result.getFieldError("id").getDefaultMessage()), HttpStatus.CONFLICT);
		}
		
        shotService.save(shot);
  
        HttpHeaders headers = new HttpHeaders();
        
        return new ResponseEntity<String>(headers, HttpStatus.CREATED);
    }
	
	@GetMapping(value = "/bookmarks")
	public ResponseEntity<Page<Shot>> findBookmarks(@RequestParam(value = "type", required = false, defaultValue = "") String type, Pageable pageable) {
		
		LocalDateTime now = LocalDateTime.now();
		
		Page<Shot> shots = new PageImpl<>(new ArrayList<Shot>(), pageable, 0);
		
		if (type.startsWith("created")) {
			
			shots = shotService.findByCreatedAtOrderByDesc(pageable);
			
		} else if(type.startsWith("latest")) {
			
			shots = shotService.findByLastMinutes(now.minusMinutes(2), now, pageable);
		}
		
        return new ResponseEntity<Page<Shot>>(shots, HttpStatus.OK);
	}
	
	@DeleteMapping(value = "/bookmarks/bookmark/{id}")
    public ResponseEntity<Shot> deleteUser(@PathVariable("id") Integer id) {
  
        Shot shot = shotService.findOne(id);
        
        if (StringUtils.isEmpty(shot)) {
            
            return new ResponseEntity<Shot>(HttpStatus.NOT_FOUND);
        }
  
        shotService.delete(shot);
        
        return new ResponseEntity<Shot>(HttpStatus.NO_CONTENT);
    }
}