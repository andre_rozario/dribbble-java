package br.com.geoambiente.repositories;

import java.time.LocalDateTime;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.geoambiente.models.Shot;

@Repository
public interface ShotRepository extends JpaRepository<Shot, Integer>, JpaSpecificationExecutor<Shot> {
	
	Page<Shot> findByOrderByCreatedAtDesc(Pageable pageable);
	
	@Query("SELECT s FROM Shot s WHERE s.createdAt BETWEEN :from AND :to ORDER BY s.createdAt DESC")
	Page<Shot> findByLastMinutes(@Param("from") LocalDateTime from, @Param("to") LocalDateTime to, Pageable pageable);
}
