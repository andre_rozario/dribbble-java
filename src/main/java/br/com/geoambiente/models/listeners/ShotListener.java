package br.com.geoambiente.models.listeners;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;

import br.com.geoambiente.models.Shot;

public class ShotListener {

	@PrePersist
	public void prePersist(Shot object) {
		
		object.setCreatedAt(LocalDateTime.now());
	}
}
