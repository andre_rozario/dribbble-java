package br.com.geoambiente.models;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.geoambiente.models.listeners.ShotListener;
import br.com.geoambiente.utils.LocalDateTimeDeserializer;
import br.com.geoambiente.utils.LocalDateTimeSerializer;
import br.com.geoambiente.validators.Unique;

@Entity
@EntityListeners(ShotListener.class)
@Table(name = "shot")
@Unique.List({
    @Unique(attributes = {"id"}, node = "id", message = "Imagem já está salva na sua galeria de favoritos!"),
})
public class Shot implements Serializable {

	private static final long serialVersionUID = -2291823540301733026L;

	@Id
	private Integer id;
	
	private String title;
	
	@Column(name = "img_normal_url")
	private String imgNormalUrl;
	
	@Column(name = "img_hidpi_url")
	private String imgHidpiUrl;
	
	@Column(name = "views_count")
	private Integer viewsCount;
	
	@Column(name = "buckets_count")
	private Integer bucketsCount;
	
	@Column(name = "likes_count")
	private Integer likesCount;
	
	@Column(name = "attachments_count")
	private Integer attachmentsCount;
	
	private String author;
	
	@JsonDeserialize(using = LocalDateTimeDeserializer.class)  
	@JsonSerialize(using = LocalDateTimeSerializer.class)
	@Column(name = "created_at", columnDefinition="TIMESTAMP")
	private LocalDateTime createdAt;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImgNormalUrl() {
		return imgNormalUrl;
	}

	public void setImgNormalUrl(String imgNormalUrl) {
		this.imgNormalUrl = imgNormalUrl;
	}

	public String getImgHidpiUrl() {
		return imgHidpiUrl;
	}

	public void setImgHidpiUrl(String imgHidpiUrl) {
		this.imgHidpiUrl = imgHidpiUrl;
	}

	public Integer getViewsCount() {
		return viewsCount;
	}

	public void setViewsCount(Integer viewsCount) {
		this.viewsCount = viewsCount;
	}

	public Integer getBucketsCount() {
		return bucketsCount;
	}

	public void setBucketsCount(Integer bucketsCount) {
		this.bucketsCount = bucketsCount;
	}

	public Integer getLikesCount() {
		return likesCount;
	}

	public void setLikesCount(Integer likesCount) {
		this.likesCount = likesCount;
	}

	public Integer getAttachmentsCount() {
		return attachmentsCount;
	}

	public void setAttachmentsCount(Integer attachmentsCount) {
		this.attachmentsCount = attachmentsCount;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
}