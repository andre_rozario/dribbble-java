package br.com.geoambiente;

import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.geoambiente.config.DesafioApplication;
import br.com.geoambiente.models.Shot;
import br.com.geoambiente.repositories.ShotRepository;
import br.com.geoambiente.services.ShotService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = DesafioApplication.class)
public class DesafioApplicationTests {
	
	private StringBuilder url;
	
	private static final String ACCESS_TOKEN ="6324df57dddd0810b38fc4b087c91a86a0a8a93cd6fabed0fd8136110f561176";
	
	@Mock
	ShotRepository shotRepository;
	
	private ShotService shotService;
	
	@Before
	public void setUp() {
		
		MockitoAnnotations.initMocks(this);
		
		shotService = new ShotService(shotRepository);
		
		this.url = new StringBuilder().append("https://api.dribbble.com/v1/shots?sort=views&page=1&access_token=").append(ACCESS_TOKEN);
	}
	
	@Test
	public void givenAccessExists_whenRequestIsExecuted_then200IsReceived() throws ClientProtocolException, IOException {
		
		// Given
		HttpUriRequest request = new HttpGet(url.toString());
		
	    // When
	    HttpResponse httpResponse = HttpClientBuilder.create().build().execute(request);
	    
	    // Then
	    assertEquals(httpResponse.getStatusLine().getStatusCode(), HttpStatus.SC_OK);
	}
	
	@Test
    public void givenRequestWithNoAcceptHeader_whenRequestIsExecuted_thenDefaultResponseContentTypeIsJson() throws ClientProtocolException, IOException {
		
		// Given
        final String jsonMimeType = "application/json";
        final HttpUriRequest request = new HttpGet(url.toString());
        
        // When
        final HttpResponse response = HttpClientBuilder.create().build().execute(request);
        
        // Then
        final String mimeType = ContentType.getOrDefault(response.getEntity()).getMimeType();
        assertEquals(jsonMimeType, mimeType);
    }
	
	@Test
	public void shouldSaveShotAsBookmark() throws Exception {
		
		Shot shot = new Shot();
		
		shot.setId(1);
		
		shotService.save(shot);
		
		Mockito.verify(shotRepository).save(shot);
	}
	
	@Test
	public void shouldDeleteShotFromBookmarks() throws Exception {
		
		Shot shot = new Shot();
		
		shot.setId(1);
		
		shotService.delete(shot);
		
		Mockito.verify(shotRepository).delete(shot);
	}
}